#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <wiringPi.h>
#include <softPwm.h>

#define IN1_PIN 1
#define IN2_PIN 4
#define IN3_PIN 5
#define IN4_PIN 6

#define MAX_SPEED 70
#define MIN_SPEED 0

#define LEFT_IR_PIN 27
#define RIGHT_IR_PIN 26

#define TRIG_PIN 28
#define ECHO_PIN 29

#define LEFT_TRACER_PIN 10
#define RIGHT_TRACER_PIN 11

void initDCMotorSmooth();
void initDCMotor();
void goForward();
void goForwardSmooth(int speed);
void goBackward();
void goBackwardSmooth(int speed);
void goLeft();
void goLeftSmooth(int speed);
void smoothLeft();
void goRight();
void goRightSmooth(int speed);
void smoothRight();
void stopDCMotor();
void stopDCMotorSmooth();
void initLineTracer();
void initIR();
void rotate();
void stopCar();

int getDistance();

void byPassObstacle();
void park();
void goBackFromPark();
void waitSuddenPedestrian();


int main (void){
if (wiringPiSetup() == -1){
return 0;
}

signal(SIGINT, stopCar);

int leftTracer, rightTracer;

initLineTracer();

int LValue, RValue;

initIR();

int count;

initDCMotor();
initDCMotorSmooth();

goForwardSmooth(1);
delay(100);

while (1) {
leftTracer = digitalRead(LEFT_TRACER_PIN);
rightTracer = digitalRead(RIGHT_TRACER_PIN);
LValue = digitalRead(LEFT_IR_PIN);
RValue = digitalRead(RIGHT_IR_PIN);
    if (LValue == 0 || RValue == 0) {
        if (count == 0) {
            count = 1;
            delay(10);
            stopDCMotor();
            initDCMotor();
            waitSuddenPedestrian();
            delay(1000);
        } else if (count == 1) {
            byPassObstacle();
            park();
            goBackFromPark();
            byPassObstacle();
        }
    }
    else if(LValue == 1 && RValue == 1)
    {
     	if (leftTracer == 1 && rightTracer == 0 )
        {
       	goLeftSmooth(1.2);
       	delay(30);
        }    

    	else if (leftTracer == 0 && rightTracer == 1 )
    	{
        goRightSmooth(1.2);
        delay(30);
        }   
 
    	else if (leftTracer == 1 && rightTracer == 1 )
    	{
        goForwardSmooth(1);
        delay(10);
    	}	

    	else if (leftTracer == 0 && rightTracer == 0)
    	{
        goForwardSmooth(1);
        delay(10);
    	}
    }

}

stopDCMotor();

return 0;

}

void initDCMotor()
{
pinMode(IN1_PIN, OUTPUT);
pinMode(IN2_PIN, OUTPUT);
pinMode(IN3_PIN, OUTPUT);
pinMode(IN4_PIN, OUTPUT);

digitalWrite(IN1_PIN, HIGH);
digitalWrite(IN2_PIN, HIGH);
digitalWrite(IN3_PIN, HIGH);
digitalWrite(IN4_PIN, HIGH);
}

void initDCMotorSmooth()
{
pinMode(IN1_PIN, SOFT_PWM_OUTPUT);
pinMode(IN2_PIN, SOFT_PWM_OUTPUT);
pinMode(IN3_PIN, SOFT_PWM_OUTPUT);
pinMode(IN4_PIN, SOFT_PWM_OUTPUT);

softPwmCreate(IN1_PIN, MIN_SPEED, MAX_SPEED);
softPwmCreate(IN2_PIN, MIN_SPEED, MAX_SPEED);
softPwmCreate(IN3_PIN, MIN_SPEED, MAX_SPEED);
softPwmCreate(IN4_PIN, MIN_SPEED, MAX_SPEED);
}

void goForward()
{
digitalWrite(IN1_PIN, HIGH);
digitalWrite(IN2_PIN, LOW);
digitalWrite(IN3_PIN, HIGH);
digitalWrite(IN4_PIN, LOW);
printf("Forward\n");
}

void goForwardSmooth(int speed)
{
softPwmWrite(IN1_PIN, MAX_SPEED/speed);
softPwmWrite(IN2_PIN, MIN_SPEED);
softPwmWrite(IN3_PIN, MAX_SPEED/speed);
softPwmWrite(IN4_PIN, MIN_SPEED);
printf("Smooth Forward\n");
}

void goBackward()
{
digitalWrite(IN1_PIN, LOW);
digitalWrite(IN2_PIN, HIGH);
digitalWrite(IN3_PIN, LOW);
digitalWrite(IN4_PIN, HIGH);
printf("Backward\n");
}

void goBackwardSmooth(int speed)
{
softPwmWrite(IN1_PIN, MIN_SPEED);
softPwmWrite(IN2_PIN, MAX_SPEED/speed);
softPwmWrite(IN3_PIN, MIN_SPEED);
softPwmWrite(IN4_PIN, MAX_SPEED/speed);
printf("Smooth Backward\n");
}

void goLeft()
{
digitalWrite(IN1_PIN, LOW);
digitalWrite(IN2_PIN, HIGH);
digitalWrite(IN3_PIN, HIGH);
digitalWrite(IN4_PIN, LOW);
printf("Left\n");
}

void goLeftSmooth(int speed)
{
softPwmWrite(IN1_PIN, MIN_SPEED);
softPwmWrite(IN2_PIN, MAX_SPEED/speed);
softPwmWrite(IN3_PIN, MAX_SPEED/speed);
softPwmWrite(IN4_PIN, MIN_SPEED);
printf("Left Smooth\n");
}

void goRight()
{
digitalWrite(IN1_PIN, HIGH);
digitalWrite(IN2_PIN, LOW);
digitalWrite(IN3_PIN, LOW);
digitalWrite(IN4_PIN, HIGH);
printf("Right\n");
}

void goRightSmooth(int speed)
{
softPwmWrite(IN1_PIN, MAX_SPEED/speed);
softPwmWrite(IN2_PIN, MIN_SPEED);
softPwmWrite(IN3_PIN, MIN_SPEED);
softPwmWrite(IN4_PIN, MAX_SPEED/speed);
printf("Right Smooth\n");
}

void stopDCMotor()
{
digitalWrite(IN1_PIN, LOW);
digitalWrite(IN2_PIN, LOW);
digitalWrite(IN3_PIN, LOW);
digitalWrite(IN4_PIN, LOW);
printf("Stop\n");
}

void stopDCMotorSmooth()
{
softPwmWrite(IN1_PIN, MIN_SPEED);
softPwmWrite(IN2_PIN, MIN_SPEED);
softPwmWrite(IN3_PIN, MIN_SPEED);
softPwmWrite(IN4_PIN, MIN_SPEED);
printf("Stop Smooth\n");
}

int getDistance()
{
int start_time=0, end_time=0;
float distance=0;
digitalWrite(TRIG_PIN, LOW) ;
delay(500) ;
digitalWrite(TRIG_PIN, HIGH) ;
delayMicroseconds(10) ;
digitalWrite(TRIG_PIN, LOW) ;
while (digitalRead(ECHO_PIN) == 0) ;
start_time = micros() ;
while (digitalRead(ECHO_PIN) == 1) ;
end_time = micros() ;
distance = (end_time - start_time) / 29. / 2. ;
return (int)distance;
}

void smoothLeft()
{
softPwmWrite(IN1_PIN, MAX_SPEED/8);
softPwmWrite(IN2_PIN, MIN_SPEED);
softPwmWrite(IN3_PIN, MAX_SPEED);
softPwmWrite(IN4_PIN, MIN_SPEED);
}

void smoothRight()
{
softPwmWrite(IN1_PIN, MAX_SPEED);
softPwmWrite(IN2_PIN, MIN_SPEED);
softPwmWrite(IN3_PIN, MAX_SPEED/8);
softPwmWrite(IN4_PIN, MIN_SPEED);
}

void initLineTracer()
{
pinMode(LEFT_TRACER_PIN, INPUT);
pinMode(RIGHT_TRACER_PIN, INPUT);
}

void initIR()
{
pinMode(LEFT_IR_PIN, INPUT);
pinMode(RIGHT_IR_PIN, INPUT);
}

void stopCar()
{
stopDCMotor();
delay(1);
exit(0);
}

void byPassObstacle()
{
    int LValue, RValue;
    int leftTracer, rightTracer;
    goBackward();
    delay(300);
    goRight();
    delay(500);
    goForward();
    delay(500);

    // Go forward till the line
    while (1) {
        leftTracer = digitalRead(LEFT_TRACER_PIN);
        rightTracer = digitalRead(RIGHT_TRACER_PIN);
        
        if (leftTracer == 0 || rightTracer == 0) {
            goBackward();
            delay(100);
            stopDCMotor();
            initDCMotor();
            break;
        } 
    }

    goLeft();
    delay(300);
    goForward();

    // Go forward till the second line
    while (1) {
        leftTracer = digitalRead(LEFT_TRACER_PIN);
        rightTracer = digitalRead(RIGHT_TRACER_PIN);
        
        if (leftTracer == 1 && rightTracer == 0) {
            goLeft();
            delay(20);
        } else if (rightTracer == 1 && leftTracer == 0) {
            goRight();
            delay(20);
        } else if (rightTracer == 1 && leftTracer == 1) {
            goForward();
        } else if (rightTracer == 0 && leftTracer == 0) {          
            goBackward();
            delay(200);
            stopDCMotor();
            initDCMotor();
            break;
        }
    }

    goLeft();
    delay(500);
    goForward();
    delay(500);

    // Go forward till the line
    while (1) {
        leftTracer = digitalRead(LEFT_TRACER_PIN);
        rightTracer = digitalRead(RIGHT_TRACER_PIN);
        
        if (leftTracer == 0 || rightTracer == 0) {
            goBackward();
            delay(100);
            stopDCMotor();
            initDCMotor();
            break;
        } 
    }

    goRight();
    delay(500);
    goForward();
}

void park()
{
    int leftTracer, rightTracer;
    
    goForward();

    // Go forward till the line
    while (1) {
        leftTracer = digitalRead(LEFT_TRACER_PIN);
        rightTracer = digitalRead(RIGHT_TRACER_PIN);

        if (leftTracer == 1 && rightTracer == 0) {
            goLeft();
            delay(20);
        } else if (rightTracer == 1 && leftTracer == 0) {
            goRight();
            delay(20);
        } else if (rightTracer == 1 && leftTracer == 1) {
            goForward();
        } else if (rightTracer == 0 && leftTracer == 0) {          
            goBackward();
            delay(100);
            stopDCMotor();
            initDCMotor();
            break;
        }
    }

    goLeft();
    delay(500);
    goForward();

    // Go forward till the second line
    while (1) {
        leftTracer = digitalRead(LEFT_TRACER_PIN);
        rightTracer = digitalRead(RIGHT_TRACER_PIN);
        
        if (leftTracer == 0 || rightTracer == 0) {
            delay(200);
            stopDCMotor();
            initDCMotor();
            delay(500);
        break;
        } 
    }
}

void goBackFromPark()
{
    int LValue, RValue;
    int leftTracer, rightTracer;
    
    goBackward();
    delay(1000);
    goLeft();
    delay(500);
    goForward();

    while (1) {
        LValue = digitalRead(LEFT_IR_PIN);
        RValue = digitalRead(RIGHT_IR_PIN);

        if (LValue == 0 || RValue == 0) {
            stopDCMotor();
            initDCMotor();
            break;
        }

        leftTracer = digitalRead(LEFT_TRACER_PIN);
        rightTracer = digitalRead(RIGHT_TRACER_PIN);

        if (leftTracer == 1 && rightTracer == 0) {
            goLeft();
            delay(20);
        } else if (rightTracer == 1 && leftTracer == 0) {
            goRight();
            delay(20);
        } else if (rightTracer == 1 && leftTracer == 1) {
            goForward();
        } else if (rightTracer == 0 && leftTracer == 0) {          
            goBackward();
            delay(100);
            goLeft();
            delay(200);
            goForward();
        }
    }
}

void waitSuddenPedestrian() 
{
    int LValue, RValue;

    // Go forward till the obstacle
    while (1) {
        delay(500);
        LValue = digitalRead(LEFT_IR_PIN);
        RValue = digitalRead(RIGHT_IR_PIN);

        if (LValue == 1 && RValue == 1) {
            break;
        }
    }
}