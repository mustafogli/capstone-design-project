#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <wiringPi.h>
#include <softPwm.h>

#define IN1_PIN 1
#define IN2_PIN 4
#define IN3_PIN 5
#define IN4_PIN 6

#define MAX_SPEED 60
#define MIN_SPEED 0

#define LEFT_IR_PIN 27
#define RIGHT_IR_PIN 26

#define TRIG_PIN 28
#define ECHO_PIN 29

#define LEFT_TRACER_PIN 10
#define RIGHT_TRACER_PIN 11

void initDCMotorSmooth();
void initDCMotor();
void goForward();
void goForwardSmooth();
void goBackward();
void goBackwardSmooth();
void goLeft();
void goLeftSmooth();
void smoothLeft();
void goRight();
void goRightSmooth();
void smoothRight();
void stopDCMotor();
void stopDCMotorSmooth();
void initLineTracer();
void initIR();
void rotate();

void stopCar();

int getDistance();

int main (void){
if (wiringPiSetup() == -1){
return 0;
}

signal(SIGINT, stopCar);

pinMode(TRIG_PIN, OUTPUT);
pinMode(ECHO_PIN, INPUT);

int leftTracer, rightTracer;

initDCMotor();
initDCMotorSmooth();

initLineTracer();
int LValue, RValue;
initIR();



int cnt = 0;
while (1) {
        leftTracer = digitalRead(LEFT_TRACER_PIN);
        rightTracer = digitalRead(RIGHT_TRACER_PIN);
	LValue = digitalRead(LEFT_IR_PIN);
	RValue = digitalRead(RIGHT_IR_PIN);

if(LValue == 0 && RValue == 0){
printf("Both\n");
stopDCMotor();
continue;
}

else if(LValue == 1 && RValue == 1){
       	
 if (leftTracer == 1 && rightTracer == 0) {
            printf("Left\n");
            smoothLeft();
            delay(30);
        } else if (rightTracer == 1 && leftTracer == 0) {
            printf("Right\n");
            smoothRight();
            delay(30);
        } else if (rightTracer == 1 && leftTracer == 1) {
            printf("Both\n");
            goForwardSmooth();
            cnt++;
	    printf("Counter: %d\n", cnt);
            
            if (cnt == 4) {
                stopDCMotor();
		rotate();
                delay(1000);
                continue;
            }

            if (cnt >= 7) {
                stopDCMotor();
                break;
            }
	    delay(200);
        } else if (rightTracer == 0 && leftTracer == 0) {
            printf("No\n");
            goForwardSmooth();
            delay(50);
        }
    }
}
    
    return 0;
}

void initDCMotor()
{
pinMode(IN1_PIN, OUTPUT);
pinMode(IN2_PIN, OUTPUT);
pinMode(IN3_PIN, OUTPUT);
pinMode(IN4_PIN, OUTPUT);

digitalWrite(IN1_PIN, HIGH);
digitalWrite(IN2_PIN, HIGH);
digitalWrite(IN3_PIN, HIGH);
digitalWrite(IN4_PIN, HIGH);
}

void initDCMotorSmooth()
{
pinMode(IN1_PIN, SOFT_PWM_OUTPUT);
pinMode(IN2_PIN, SOFT_PWM_OUTPUT);
pinMode(IN3_PIN, SOFT_PWM_OUTPUT);
pinMode(IN4_PIN, SOFT_PWM_OUTPUT);

softPwmCreate(IN1_PIN, MIN_SPEED, MAX_SPEED);
softPwmCreate(IN2_PIN, MIN_SPEED, MAX_SPEED);
softPwmCreate(IN3_PIN, MIN_SPEED, MAX_SPEED);
softPwmCreate(IN4_PIN, MIN_SPEED, MAX_SPEED);
}

void goForward()
{
digitalWrite(IN1_PIN, HIGH);
digitalWrite(IN2_PIN, LOW);
digitalWrite(IN3_PIN, HIGH);
digitalWrite(IN4_PIN, LOW);
printf("Forward\n");
}

void goForwardSmooth()
{
softPwmWrite(IN1_PIN, MAX_SPEED);
softPwmWrite(IN2_PIN, MIN_SPEED);
softPwmWrite(IN3_PIN, MAX_SPEED);
softPwmWrite(IN4_PIN, MIN_SPEED);
printf("Smooth Forward\n");
}

void goBackward()
{
digitalWrite(IN1_PIN, LOW);
digitalWrite(IN2_PIN, HIGH);
digitalWrite(IN3_PIN, LOW);
digitalWrite(IN4_PIN, HIGH);
printf("Backward\n");
}

void goBackwardSmooth()
{
softPwmWrite(IN1_PIN, MIN_SPEED);
softPwmWrite(IN2_PIN, MAX_SPEED);
softPwmWrite(IN3_PIN, MIN_SPEED);
softPwmWrite(IN4_PIN, MAX_SPEED);
printf("Smooth Backward\n");
}

void goLeft()
{
digitalWrite(IN1_PIN, LOW);
digitalWrite(IN2_PIN, HIGH);
digitalWrite(IN3_PIN, HIGH);
digitalWrite(IN4_PIN, LOW);
printf("Left\n");
}

void goLeftSmooth()
{
softPwmWrite(IN1_PIN, MIN_SPEED);
softPwmWrite(IN2_PIN, 150);
softPwmWrite(IN3_PIN, 150);
softPwmWrite(IN4_PIN, MIN_SPEED);
printf("Left Smooth\n");
}

void goRight()
{
digitalWrite(IN1_PIN, HIGH);
digitalWrite(IN2_PIN, LOW);
digitalWrite(IN3_PIN, LOW);
digitalWrite(IN4_PIN, HIGH);
printf("Right\n");
}

void goRightSmooth()
{
softPwmWrite(IN1_PIN, 150);
softPwmWrite(IN2_PIN, MIN_SPEED);
softPwmWrite(IN3_PIN, MIN_SPEED);
softPwmWrite(IN4_PIN, 150);
printf("Right Smooth\n");
}

void stopDCMotor()
{
digitalWrite(IN1_PIN, LOW);
digitalWrite(IN2_PIN, LOW);
digitalWrite(IN3_PIN, LOW);
digitalWrite(IN4_PIN, LOW);
printf("Stop\n");
}

void stopDCMotorSmooth()
{
softPwmWrite(IN1_PIN, MIN_SPEED);
softPwmWrite(IN2_PIN, MIN_SPEED);
softPwmWrite(IN3_PIN, MIN_SPEED);
softPwmWrite(IN4_PIN, MIN_SPEED);
printf("Stop Smooth\n");
}

int getDistance()
{
int start_time=0, end_time=0;
float distance=0;
digitalWrite(TRIG_PIN, LOW) ;
delay(500) ;
digitalWrite(TRIG_PIN, HIGH) ;
delayMicroseconds(10) ;
digitalWrite(TRIG_PIN, LOW) ;
while (digitalRead(ECHO_PIN) == 0) ;
start_time = micros() ;
while (digitalRead(ECHO_PIN) == 1) ;
end_time = micros() ;
distance = (end_time - start_time) / 29. / 2. ;
return (int)distance;
}

void smoothLeft()
{
softPwmWrite(IN1_PIN, MAX_SPEED/8);
softPwmWrite(IN2_PIN, MIN_SPEED);
softPwmWrite(IN3_PIN, MAX_SPEED);
softPwmWrite(IN4_PIN, MIN_SPEED);
}

void smoothRight()
{
softPwmWrite(IN1_PIN, MAX_SPEED);
softPwmWrite(IN2_PIN, MIN_SPEED);
softPwmWrite(IN3_PIN, MAX_SPEED/8);
softPwmWrite(IN4_PIN, MIN_SPEED);
}

void initLineTracer()
{
pinMode(LEFT_TRACER_PIN, INPUT);
pinMode(RIGHT_TRACER_PIN, INPUT);
}

void initIR()
{
pinMode(LEFT_IR_PIN, INPUT);
pinMode(RIGHT_IR_PIN, INPUT);
}

void rotate()
{
	softPwmWrite(IN1_PIN, MIN_SPEED);
	softPwmWrite(IN2_PIN, MAX_SPEED);
	softPwmWrite(IN3_PIN, MAX_SPEED);
	softPwmWrite(IN4_PIN, MIN_SPEED);

}

void stopCar()
{
stopDCMotor();
delay(1);
exit(0);
}